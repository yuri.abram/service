package service

import (
	"context"
	"github.com/go-chi/jwtauth/v5"
)

type Auther interface {
	Login(ctx context.Context, user UserReq) (token string, err error)
	Register(ctx context.Context, user UserReq) error
}

type UserReq struct {
	Username string `json:"username" example:"admin"`
	Password string `json:"password" example:"pass"`
}

type UsersDB struct {
	users map[string]string
}

func NewAuth() *UsersDB {
	return &UsersDB{users: make(map[string]string)}
}

var TokenAuth = jwtauth.New("HS256", []byte("secret"), nil)
