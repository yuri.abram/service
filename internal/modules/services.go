package modules

import (
	"gitlab/controller/internal/infrastructure/component"
	aservice "gitlab/controller/internal/modules/auth/service"
	gservice "gitlab/controller/internal/modules/geo/service"
)

type Services struct {
	GeoService gservice.GeoServicer
	Auth       aservice.Auther
}

func NewServices(components *component.Components) *Services {
	geoserv := gservice.NewGeoServicer(components.Logger)
	aserv := aservice.NewAuth()
	return &Services{
		GeoService: geoserv,
		Auth:       aserv,
	}
}
