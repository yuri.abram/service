package service

import (
	"context"
	"go.uber.org/zap"
)

type GeoServicer interface {
	Search(ctx context.Context, rq *SearchRequest) SearchResponse
	Geocode(ctx context.Context, rq *GeocodeRequest) GeocodeResponse
}

type GeoService struct {
	logger *zap.Logger
}

func NewGeoServicer(logger *zap.Logger) *GeoService {
	return &GeoService{logger}
}
